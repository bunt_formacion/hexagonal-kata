# Hexagonal Kata

Esta kata está basada en ["The Birthday greetings kata"](http://matteo.vaccari.name/blog/archives/154) por Matteo Vaccari.

## Objetivos

Esta kata está dirigida a mejorar nuestros conocimientos de diseño aplicando [Arquitectura Hexagonal](http://alistair.cockburn.us/Hexagonal+architecture), o Puertos y Adaptadores.

Como todas las katas de programación, el objetivo es trabajar sin las limitaciones y urgencias del día a día, en un problema con una alcance bien definido, meditando cada paso que se da y atendiendo al detalle.

Durante el ejercicio, se ha de fomentar el debate de todas las decisiones que se toman para que sean entendidos los porqués y los cómos, como si quisieramos hacer el código fuente perfecto.

## Alcance de nuestra aplicación

Vamos a desarrollar una aplicación que envíe emails de felicitaciones de cumpleaños a todos los empleados de una empresa.

Cada día, nuestra aplicación leerá de un fichero externo los datos de los empleados, de modo que sabrá cuales de ellos cumplen años hoy, para enviarles un email con su nombre y su edad.

El fichero con los datos de los empleados sigue el estandar CSV y tiene el siguiente formato:

    apellidos, nombre, fecha_nacimiento, email
    Artola García, Luis, 1981-03-31, luis.artola@buntplanet.com
    Guitierrez Almazor, Guillermo, 1980-05-02, guillermo.gutierrez@buntplanet.com

La plantilla de email que se use, deberá producir emails parecidos al siguiente, con los datos que correspondan:

    Asunto: ¡Feliz 34º cumpleaños!

    ¡Feliz cumpleaños, Guillermo!

El programa que se entrege, deberá poder ser invocado desde el comando de línea, por medio de una llamada a un fichero jar, indicando la ruta al fichero externo con los datos de los empleados

Si quieres hacerlo un poco más difícil, puedes incluir nuevos comportamientos al ejercicio, pero asegúrate de hacerlo siempre obedeciendo a los requisitos de diseño que vamos a describir más adelante en este documento. Nosotros te sugerimos las siguientes funcionalidades extra:

 - Que en el asunto del mensaje se usen correctamente los carácteres º y ª en función de la edad del empleado
 - Que si el empleado ha nacido en 29 de febrero, su felicitación sea enviada el 28 de febrero si el año actual no es bisiesto.

## Requisitos de diseño

Realizar un buen diseño es un medio para conseguir un fin. Queremos que la solución siempre sea:

 - Testable: deberíamos poder testear la lógica interna de la aplicación sin tener que enviar un email.
 - Flexible: asumimos que los datos de entrada pueden evolucionar con el tiempo y provenir de una base de datos o un servicio web. También asumimos que el canal de envío podría cambiar por una red social o un servicio de mensajería distinto.
 - Bien diseñado: el código de infraestructura debe estar claramente separado del código de la lógica de negocio de nuestra aplicación.

Como queremos que la aplicación sea ejecutada desde el comando de línea, no vamos a complicarnos la vida y la aplicación debería lanzarse desde un bloque estático main tradicional de java parecido a este:

```
public static void main(String[] args) {
    ...
    BirthdayService birthdayService = new BirthdayService(employeeRepository, emailService);
    birthdayService.sendGreetings(today());
}
```

## Testabilidad

Un test no será considerado unitario si:

 - interactúa con una base de datos
 - se comunica a través de una red
 - interactúa con el sistema de ficheros
 - requires de cambios en el entorno para ser ejecutado (ficheros de configuración, comentar una línea, etc.)

Los tests de integración tienen su lugar en esta arquitectura, pero deben estar claramente identificados para que podamos ejecutarlos por separado. Una regla general para diferenciar un test unitario de uno de integración podría ser la siguiente:

 - Es muy rápido: se deberían ejecutar miles de tests por segundo
 - Es fiable: no debería fallar por fallos en sistemas externos

Para que nuestro código sea más testable, se puede emplear la inyección de dependencias. Esto signfica que un objeto nunca debe instanciar objetos de otras clases. En vez de eso, organizamos la creación de objetos del siguiente modo:

 - Si un objeto necesita interactúar con otro, debe recibirlo a través de su constructor ya instanciado.
 - Toda la instanciación de objetos se realiza en un mismo punto, de manera concentrada donde quedan explicitadas las dependencias entre los objetos de nuestra aplicación.

*Pregunta extra: Es fácil saber cuántas clases tiene una aplicación, pero ¿cuántos objetos tiene en tiempo de ejecución?*

## Diseño por capas vs Arquitectura Hexagonal

Tradicionalmente separamos las responsabilidades de nuestras aplicaciones en tres capas: persistencia, dominio y presentación:

    +--------------+
    | presentación |
    |--------------|
    |   dominio    |
    |--------------|
    | persistencia |
    +--------------+

Según este diseño:

 - Una capa lo forma un conjunto de clases
 - Una clase puede hacer referencia a otras clases por debajo suyo, pero no a las que están por encima de ella

Este diseño tiene algunas pegas:

 - Se asume que solo hay comunicación con otras dos entidades fuera de la aplicación: el usuario (por la capa de presentación) y la base de datos (por la de persistencia). Esto no siempre es cierto.
 - El código de dominio se acopla con el de persistencia de manera que queda "intoxicado" por APIs externas.
 - Resulta difícil testear la lógica del dominio asilada de la base de datos.

La **Arquitectura Hexagonal**, en cambio, trata todos los sistemas externos del mismo modo. Modelamos nuestro sistema como un núcleo que implementa, el dominio de nuestra aplicación, con distintas facetas, cada una "mirando" a un sistema externo.

Cada sistema externo (base de datos, interfaz gráfica, web, consola, etc.), está detrás de una fachada que:

 - Provee de una visión simplificada del sistema externo, solo con las operaciones de nuestro sistema que tienen que ver con el sistema externo.
 - Es expresada en función del dominio.

En el dominio del sistema no hay capas que definan o limiten la capacidad de interactuar entre sus clases. Del mismo modo, todo código no de dominio, depende de el.

Por ejemplo, para aislarnos totalmente de la base de datos, el dominio puede definir un Repositorio que devuelve objetos de dominio. La interfaz de dicho Repositorio (su puerto) se define en el dominio y se implementa en el adaptador de la base de datos usando la sintáxis especifica del motor de persistencia que estemos usando.

